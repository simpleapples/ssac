//
//  HelpDetailController.h
//  Nav
//
//  Created by Dave Mark on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpDetailController : UIViewController {
    UITextView    *label;
    NSString   *message;
}
@property (nonatomic, retain) IBOutlet UITextView *label;
@property (nonatomic, copy) NSString *message;
@end
