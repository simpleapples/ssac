//
//  FirstLevelViewController.m
//  Nav
//
//  Created by Dave Mark on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FirstLevelViewController.h"
#import "SecondLevelViewController.h"
#import "DisclosureButtonController.h"
#import "HelpDetailController.h"
#import "DownloadManager.h"
#import "NSURL+Download.h"
#import "NavAppDelegate.h"

@implementation FirstLevelViewController
{
    UILabel *versionLabel;
    UIScrollView *_backScroll;
    UIImageView *versionLabelBackground;
}
@synthesize controllers;

#define kDocumentFolder					[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] 
#define kCerUrl @"http://m.buctsu.com/iphone/certification.plist"
#define kDataUrl @"http://m.buctsu.com/iphone/database.plist"

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void) downloadManagerDataDownloadFinished: (NSString *) fileName
{
    
	// 返回的fileName为保存的路径
	
}
- (void) downloadManagerDataDownloadFailed: (NSString *) reason
{
	
}

-(void)clickLeftButton  
{  
    //读入certification文件版本号
    NSString *versionValue = [[NSString alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[kCerUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] encoding:NSUTF8StringEncoding];
    //读入database版本号
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *filePath = [docDir stringByAppendingPathComponent:@"database.plist"];
    NSDictionary *dicts1 = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    NSArray *versionLabel1 = [dicts1 objectForKey:@"版本号"];
    NSString *oldVersionValue = [[NSString alloc] initWithFormat:
                              @"%@",[versionLabel1 objectAtIndex:0]];
    NSInteger oldVersion = [oldVersionValue intValue];
    NSInteger version = [versionValue intValue];
    //比较database版本号和certification版本号
    if (version == 0) {
        //弹出窗口提示升级
        NSString *title = [[NSString alloc] initWithFormat:@"网络异常"];
        NSString *message = [[NSString alloc] initWithFormat:
                             @"请检查网络连接"];
        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"确定"
                                               otherButtonTitles:nil, nil
                               ];
        [alert1 setTag:8];
        [alert1 show];
    } else {
        if(version > oldVersion)
        {
            //弹出窗口提示升级
            NSString *title = [[NSString alloc] initWithFormat:@"升级数据库"];
            NSString *message = [[NSString alloc] initWithFormat:
                                 @"有可用的更新，是否升级？"];
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:title
                                                             message:message
                                                            delegate:self
                                                   cancelButtonTitle:@"取消"
                                                   otherButtonTitles:@"确定", nil
                                   ];
            [alert1 setTag:1];
            [alert1 show];
        }
        else
        {
            //弹出窗口提示无需升级
            NSString *title2 = [[NSString alloc] initWithFormat:@"不需要升级的哦亲"];
            NSString *message2 = [[NSString alloc] initWithFormat:
                                  @"您的数据库是最新的"];
            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:title2
                                                             message:message2
                                                            delegate:nil
                                                   cancelButtonTitle:@"确定"
                                                   otherButtonTitles:nil];
            [alert2 setTag:2];
            [alert2 show];
        }
    }
}  
-(void)alertView:(UIAlertView *) inAlertView clickedButtonAtIndex:(NSInteger) buttonIndex//响应按钮点击
{
	switch ([inAlertView tag]) {
        case 1:{
            switch (buttonIndex) {
                case 1:
                {
                    [self updateDatabase];
                    break;
                }
                default:
                {
                    break;
                }
            }
            default:
            {
                break;
            }
        }
    }
}   
-(void)alertView:(UIAlertView *) inAlertView didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    switch ([inAlertView tag]) {
        case 1:
            break;
        case 2:
            break;
        case 4:
        {
            if (buttonIndex == 0) {
                [self readVersion];
            }
        }
        break;
        default:
        break;
    }
}

-(void)clickRightButton  
{  
    HelpDetailController *childHelpView = [[HelpDetailController alloc]
                                           initWithNibName:@"DisclosureDetail" bundle:nil];
    //childHelpView.title = @"Disclosure Button Pressed";
    //NSUInteger row = [indexPath row];
    //NSString *selectedMovie = [list objectAtIndex:row];
    NSString *detailMessage = [[NSString alloc] initWithFormat:@"You\npressed \nthe disclosureYou pressed the disclosure button for button for"];
    NSString *title = @"关于";
    childHelpView.message = detailMessage;
    childHelpView.title = title;
    [self.navigationController pushViewController:childHelpView
                                         animated:YES];
}  

- (void)viewDidLoad {
    NavAppDelegate *appdelegate = [UIApplication sharedApplication].delegate;
    appdelegate.isDownloadSucc = NO;
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:@"检查更新"     
                                                                   style:UIBarButtonItemStyleBordered     
                                                                  target:self     
                                                                  action:@selector(clickLeftButton)
                                   ];   
    self.navigationItem.leftBarButtonItem = leftButton;
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"关于"     
                                                                   style:UIBarButtonItemStyleBordered     
                                                                  target:self     
                                                                  action:@selector(clickRightButton)
                                   ];   
    self.navigationItem.rightBarButtonItem = rightButton;
	self.navigationItem.title = @"选择星期";
    
    versionLabelBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"versionlabelbackground.png"]];
    versionLabelBackground.frame = CGRectMake(0, self.view.frame.size.height - 10, self.view.frame.size.width, 30);
    versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.view.frame.size.width - 40, 30)];
    versionLabel.font = [UIFont systemFontOfSize:14];
    versionLabel.backgroundColor = [UIColor clearColor];
    [versionLabelBackground addSubview:versionLabel];
    [self.navigationController.view addSubview:versionLabelBackground];
    
    [self readVersion];
    
	NSMutableArray *array = [[NSMutableArray alloc] init];
	
	// Disclosure Button
    DisclosureButtonController *disclosureButtonController =
	    [[DisclosureButtonController alloc]
	    initWithStyle:UITableViewStylePlain];
    disclosureButtonController.title = @"星期一";
    disclosureButtonController.rowImage = [UIImage
	    imageNamed:@"disclosureButtonControllerIcon.png"];
    [array addObject:disclosureButtonController];
    
    // Disclosure Button
    DisclosureButtonController *disclosureButtonController1 =
    [[DisclosureButtonController alloc]
     initWithStyle:UITableViewStylePlain];
    disclosureButtonController1.title = @"星期二";
    disclosureButtonController1.rowImage = [UIImage
                                           imageNamed:@"disclosureButtonControllerIcon.png"];
    [array addObject:disclosureButtonController1];
    
    // Disclosure Button
    DisclosureButtonController *disclosureButtonController2 =
    [[DisclosureButtonController alloc]
     initWithStyle:UITableViewStylePlain];
    disclosureButtonController2.title = @"星期三";
    disclosureButtonController2.rowImage = [UIImage
                                            imageNamed:@"disclosureButtonControllerIcon.png"];
    [array addObject:disclosureButtonController2];
    
    // Disclosure Button
    DisclosureButtonController *disclosureButtonController3 =
    [[DisclosureButtonController alloc]
     initWithStyle:UITableViewStylePlain];
    disclosureButtonController3.title = @"星期四";
    disclosureButtonController3.rowImage = [UIImage
                                            imageNamed:@"disclosureButtonControllerIcon.png"];
    [array addObject:disclosureButtonController3];
    
    // Disclosure Button
    DisclosureButtonController *disclosureButtonController4 =
    [[DisclosureButtonController alloc]
     initWithStyle:UITableViewStylePlain];
    disclosureButtonController4.title = @"星期五";
    disclosureButtonController4.rowImage = [UIImage
                                            imageNamed:@"disclosureButtonControllerIcon.png"];
    [array addObject:disclosureButtonController4];
    
    // Disclosure Button
    DisclosureButtonController *disclosureButtonController5 =
    [[DisclosureButtonController alloc]
     initWithStyle:UITableViewStylePlain];
    disclosureButtonController5.title = @"星期六";
    disclosureButtonController5.rowImage = [UIImage
                                            imageNamed:@"disclosureButtonControllerIcon.png"];
    [array addObject:disclosureButtonController5];
    
    // Disclosure Button
    DisclosureButtonController *disclosureButtonController6 =
    [[DisclosureButtonController alloc]
     initWithStyle:UITableViewStylePlain];
    disclosureButtonController6.title = @"星期日";
    disclosureButtonController6.rowImage = [UIImage
                                            imageNamed:@"disclosureButtonControllerIcon.png"];
    [array addObject:disclosureButtonController6];

	self.controllers = array;
    
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    versionLabel.hidden = NO;
    versionLabelBackground.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    versionLabel.hidden = YES;
    versionLabelBackground.hidden = YES;
}

- (void)viewDidUnload {
	self.controllers = nil;
	[super viewDidUnload];
}

#pragma mark -
#pragma mark Table Data Source Methods
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.controllers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *FirstLevelCell= @"FirstLevelCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             FirstLevelCell];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
				 initWithStyle:UITableViewCellStyleDefault
				 reuseIdentifier: FirstLevelCell];
    }
    // Configure the cell
    NSUInteger row = [indexPath row];
    SecondLevelViewController *controller =
	[controllers objectAtIndex:row];
    cell.textLabel.text = controller.title;
    cell.imageView.image = controller.rowImage;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

#pragma mark -
#pragma mark Table View Delegate Methods
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self readVersion];
    NSUInteger row = [indexPath row];
    if ([self documentFileExist:@"database.plist"]) {
        SecondLevelViewController *nextController = [self.controllers
                                                     objectAtIndex:row];
        [self.navigationController pushViewController:nextController
                                             animated:YES];
    } else {
        NSString *versionValue = [[NSString alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[kCerUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] encoding:NSUTF8StringEncoding];
        NSInteger version = [versionValue intValue];
        //比较database版本号和certification版本号
        if (version == 0) {
            NSString *title = [[NSString alloc] initWithFormat:@"网络异常"];
            NSString *message = [[NSString alloc] initWithFormat:
                                 @"请检查网络连接"];
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:title
                                                             message:message
                                                            delegate:self
                                                   cancelButtonTitle:@"确定"
                                                   otherButtonTitles:nil, nil
                                   ];
            [alert1 setTag:8];
            [alert1 show];
        } else {
            NSString *title = [[NSString alloc] initWithFormat:@"下载数据库"];
            NSString *message = [[NSString alloc] initWithFormat:
                                 @"请下载数据库"];
            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:title
                                                             message:message
                                                            delegate:self
                                                   cancelButtonTitle:@"取消"
                                                   otherButtonTitles:@"确定", nil
                                   ];
            [alert1 setTag:1];
            [alert1 show];
        }
    }
}

- (void)readVersion
{
    // 当前版本
    
    if ([self documentFileExist:@"database.plist"]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDir = [paths objectAtIndex:0];
        NSString *filePath = [docDir stringByAppendingPathComponent:@"database.plist"];
        NSDictionary *dicts1 = [[NSDictionary alloc] initWithContentsOfFile:filePath];
        NSArray *versionLabel1 = [dicts1 objectForKey:@"版本号"];
        NSString *versionString = [[NSString alloc] initWithFormat:
                                   @"%@",[versionLabel1 objectAtIndex:0]];
        NSString *preString = [[NSString alloc] initWithFormat:@"%@",[versionString substringToIndex:4]];
        NSString *subString = [[NSString alloc] initWithFormat:@"%@",[versionString substringFromIndex:5]];
        NSInteger subInteger = [subString intValue];
        NSInteger preInteger = [preString intValue];
        NSInteger year1;
        NSInteger year2;
        NSString *versionDisplay = [[NSString alloc] init];
        NSLog(@"%d %d",preInteger, subInteger);
        if ( 201 < subInteger && subInteger < 801 ) {
            versionDisplay = @"第二学期";
            year2 = preInteger;
            year1 = preInteger - 1;
        } else {
            versionDisplay = @"第一学期";
            year2 = preInteger + 1;
            year1 = preInteger;
        }
        versionLabel.text = [[NSString alloc] initWithFormat:@"当前数据库版本为:%d-%d学年%@",year1,year2,versionDisplay];

    } else {
        versionLabel.text = [[NSString alloc] initWithFormat:@"请点击检查更新下载数据库"];
    }
}
- (BOOL)documentFileExist:(NSString *)fileName
{
    // Check the file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *filePath = [docDir stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //NSLog(@"%@",filePath);
    //NSLog(@"%d",[fileManager fileExistsAtPath:filePath]);
    return [fileManager fileExistsAtPath:filePath];
}

- (void)updateDatabase
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *filePath = [docDir stringByAppendingPathComponent:@"database.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:nil];
    download=[[DownloadManager alloc]init];
    download.title=@"正在升级数据库";
    download.fileURL=[NSURL URLWithString:kDataUrl];
    download.fileName=[kDocumentFolder stringByAppendingPathComponent:@"database.plist"];
    [download start];
    versionLabel.text = @"";
}

@end
