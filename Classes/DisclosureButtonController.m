//
//  DisclosureButtonController.m
//  Nav
//
//  Created by Dave Mark on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DisclosureButtonController.h"
#import "NavAppDelegate.h"

//#import "DisclosureDetailController.h"

@implementation DisclosureButtonController
@synthesize list;

- (void)viewDidLoad {
    NSArray *array = [[NSArray alloc] initWithObjects:@"上午第一节",
                      @"上午第二节", @"上午第三节", @"上午第四节",
                      @"上午第五节", @"下午第六节", @"下午第七节", @"下午第八节", @"下午第九节", @"下午第十节", @"晚上第十一节", @"晚上第十二节", @"晚上第十三节", nil];
    self.list = array;
    self.tableView.frame = CGRectMake( 0, 0, 320, 400);
    [super viewDidLoad];
}

- (void)viewDidUnload {
	self.list = nil;
//	[childController release], childController = nil;
}

#pragma mark -
#pragma mark Table Data Source Methods
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString * DisclosureButtonCellIdentifier =
    @"DisclosureButtonCellIdentifier";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             DisclosureButtonCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
				 initWithStyle:UITableViewCellStyleDefault
				 reuseIdentifier: DisclosureButtonCellIdentifier];
    }
    NSUInteger row = [indexPath row];
    NSString *rowString = [list objectAtIndex:row];
    cell.textLabel.text = rowString;
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    return cell;
}

//Database
/*
-(NSString *)dataFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //----------------------------------------------
    //NSString *daytitle = self.title;
    //NSUInteger row = [index row];
    //NSString *rowValue = [list objectAtIndex:row];
    //----------------------------------------------
    NSString *documentsDiretory = [[NSString alloc] initWithFormat:
                       @"%@%@", daytitle,rowValue];   
    //NSString *documentsDiretory = [paths objectAtIndex:0];
    return [documentsDiretory stringByAppendingPathComponent:@"database.plist"];
}
 */
//Database End

#pragma mark -
#pragma mark Table Delegate Methods
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSString *daytitle = self.title;
    NSUInteger row = [indexPath row];
    NSString *rowValue = [list objectAtIndex:row];
    NSString *title = [[NSString alloc] initWithFormat:
						 @"%@%@没有课的教室", daytitle,rowValue];
    //
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *filePath = [docDir stringByAppendingPathComponent:@"database.plist"];
    NSDictionary *dicts = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    NSArray *messagevalue = [dicts objectForKey:daytitle];
    NSString *message = [[NSString alloc] initWithFormat:
                        @"%@",[messagevalue objectAtIndex:row]];
    //
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
													message:message
												   delegate:nil
										  cancelButtonTitle:@"确定"
										  otherButtonTitles:nil];
    [alert show];
}
/*
- (void)tableView:(UITableView *)tableView

accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if (childController == nil) {
        childController = [[DisclosureDetailController alloc]
                           initWithNibName:@"DisclosureDetail" bundle:nil];
    }
    childController.title = @"Disclosure Button Pressed";
    NSUInteger row = [indexPath row];
    NSString *selectedMovie = [list objectAtIndex:row];
    NSString *detailMessage = [[NSString alloc]
							   initWithFormat:@"You pressed the disclosure button for %@.",
							   selectedMovie];
    childController.message = detailMessage;
    childController.title = selectedMovie;
    [detailMessage release];
    [self.navigationController pushViewController:childController
                                         animated:YES];
}
*/ 

@end
