//
//  NavAppDelegate.h
//  Nav
//
//  Created by Dave Mark on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    UINavigationController *navController;
    BOOL isDownloadSucc;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property BOOL isDownloadSucc;
@property (nonatomic, retain) IBOutlet UINavigationController
*navController;
@end