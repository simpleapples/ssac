//
//  FirstLevelViewController.h
//  Nav
//
//  Created by Dave Mark on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadManager.h"

@interface FirstLevelViewController : UITableViewController {
    NSArray *controllers;
    DownloadManager *download;
}
@property (nonatomic, retain) NSArray *controllers;

@end
