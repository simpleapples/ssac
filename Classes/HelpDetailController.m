//
//  HelpDetailController.m
//  Nav
//
//  Created by Dave Mark on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "HelpDetailController.h"
#import "QuartzCore/QuartzCore.h"

@implementation HelpDetailController
@synthesize label;
@synthesize message;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload {
	[super viewDidUnload];
}

@end
