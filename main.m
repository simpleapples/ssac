//
//  main.m
//  Nav
//
//  Created by Dave Mark on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    @autoreleasepool {
        // Code benefitting from a local autorelease pool.
        int retVal = UIApplicationMain(argc, argv, nil, nil);
        return retVal;
    }
}
